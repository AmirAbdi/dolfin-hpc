#ifndef __DOLFIN_FUNCTION_H
#define __DOLFIN_FUNCTION_H

// DOLFIN function interface

#include <dolfin/function/Function.h>
#include <dolfin/function/SpecialFunctions.h>
#include <dolfin/function/ProjectL2.h>

#endif
