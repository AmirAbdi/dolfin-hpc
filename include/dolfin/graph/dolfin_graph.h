#ifndef __DOLFIN_GRAPH_H
#define __DOLFIN_GRAPH_H

// DOLFIN graph

#include <dolfin/graph/Graph.h>
#include <dolfin/graph/GraphEditor.h>
#include <dolfin/graph/GraphPartition.h>
#include <dolfin/graph/UndirectedClique.h>
#include <dolfin/graph/DirectedClique.h>

#endif
