Guidelines for writing DOLFIN code.
-----------------------------------

In order to streamline the code a bit perhaps we should
keep sort of the same style when writing the code.

- Naming of classes:

    GridGeometry

- Naming of files:

    GridGeometry.h
    GridGeometry.cpp	

  Class files are named exactly the same as the class.
  Header files end with.h, implementation files with .cpp
  Large letter only for class files: utils.h

- Naming of variables:

    int my_variable;

- Naming of functions

    void myFunction()

- DOLFIN constants start with DOLFIN and large letters:

    DOLFIN_PI

- Header files start and end with something like

    #ifndef __GRID_GEOMETRY_H
    #define __GRID_GEOMETRY_H

    ...

    #endif

- Applying the LGPL should look something like this at the top
  of all source files:

    // Copyright (C) 2004-2005 Johan Hoffman and Anders Logg.
    // Licensed under the GNU LGPL Version 2.1.
    //
    // First added:  2004-01-01
    // Last changed: 2005-11-15
  
  or

    // Copyright (C) 2004-2005 Johan Hoffman and Anders Logg.
    // Licensed under the GNU LGPL Version 2.1.
    //
    // Modified by Andy R. Terrel, 2005.
    //
    // First added:  2004-01-01
    // Last changed: 2005-11-15

- enum types are lowercase

- The only part of PETSc that is visible outside of src/kernel/la are the functions
  Matrix::mat(), Vector::vec() etc.

- Don't #include <dolfin.h> inside the DOLFIN kernel, only include the portions of
  DOLFIN you are actually using

- Use forward declarations instead of includes in header files when possible

- Classes should have methods str() and disp(). Perhaps these should be
  made virtual in Variable

- Curly brackets following a control statement should appear in the next line,

    for(int i=1; i < 10; ++i)
    {
      cout << i   << endl;
      cout << 2*i << endl;
    }

- Make all constructors (except copy constructors) explicit if there is no particular
  reason not to do so:

    class Foo
    {
       explicit Foo(int i);
    };
