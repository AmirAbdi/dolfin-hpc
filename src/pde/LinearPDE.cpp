// Copyright (C) 2004-2007 Anders Logg.
// Licensed under the GNU LGPL Version 2.1.
//
// Modified by Garth N. Wells, 2006, 2007
//
// First added:  2004
// Last changed: 2007-12-28

#include <dolfin/la/Matrix.h>
#include <dolfin/fem/BoundaryCondition.h>
#include <dolfin/fem/Assembler.h>
#include <dolfin/la/LUSolver.h>
#include <dolfin/la/KrylovSolver.h>
#include <dolfin/function/Function.h>
#include <dolfin/function/DiscreteFunction.h>
#include <dolfin/pde/LinearPDE.h>
#include <dolfin/io/dolfin_io.h>
#include <dolfin/parameter/dolfin_parameter.h>
#include <dolfin/fem/Form.h>

using namespace dolfin;

//-----------------------------------------------------------------------------
LinearPDE::LinearPDE(Form& a, Form& L, Mesh& mesh, SolverType solver_type)
  : a(a), L(L), mesh(mesh), assembler(mesh), ksolver(solver_type), solved(false),
    cell_domains(NULL), exterior_facet_domains(NULL), interior_facet_domains(NULL)
{
  message("Creating linear PDE.");
}
//-----------------------------------------------------------------------------
LinearPDE::LinearPDE(Form& a, Form& L, Mesh& mesh, BoundaryCondition& bc, SolverType solver_type)
  : a(a), L(L), mesh(mesh), assembler(mesh), ksolver(solver_type), solved(false),
    cell_domains(NULL), exterior_facet_domains(NULL), interior_facet_domains(NULL)
{
  message("Creating linear PDE with one boundary condition.");
  bcs.push_back(&bc);
} 
//-----------------------------------------------------------------------------
LinearPDE::LinearPDE(Form& a, Form& L, Mesh& mesh, Array<BoundaryCondition*>& bcs, SolverType solver_type)
  : a(a), L(L), mesh(mesh), assembler(mesh), ksolver(solver_type), solved(false),
    cell_domains(NULL), exterior_facet_domains(NULL), interior_facet_domains(NULL)
{
  message("Creating linear PDE with %d boundary condition(s).", bcs.size());
  for (uint i = 0; i < bcs.size(); i++)
    this->bcs.push_back(bcs[i]);
}
//-----------------------------------------------------------------------------
LinearPDE::~LinearPDE()
{
  // Do nothing
}
//-----------------------------------------------------------------------------
void LinearPDE::solve(Function& u)
{
  begin("Solving linear PDE.");

  // Create matrix and vector for assembly
  if(!solved)
  {
    x = new Vector();
    u.init(mesh, *x, a, 1);
    *x = 0.0;
  }

  // Assemble linear system
  //Assembler assembler(mesh);
  if(solved)
  {
    tic();
    assembler.assemble(A, a, *cell_domains, *exterior_facet_domains, *interior_facet_domains, false);
    real tmat = toc();
    message("matrix assemble timer: %g", tmat);
    tic();
    assembler.assemble(b, L, *cell_domains, *exterior_facet_domains, *interior_facet_domains, false);
    real tvec = toc();
    message("vector assemble timer: %g", tvec);
    message("total assemble timer: %g", tmat + tvec);
  }
  else
  {
    assembler.assemble(A, a, *cell_domains, *exterior_facet_domains, *interior_facet_domains, true);
    assembler.assemble(b, L, *cell_domains, *exterior_facet_domains, *interior_facet_domains, true);
  }


  // Apply boundary conditions
  for (uint i = 0; i < bcs.size(); i++)
    bcs[i]->apply(A, b, a);

  tic();

  // Solve linear system
  ksolver.solve(A, *x, b);

  message("linear solve timer: %g", toc());

  if(!solved)
  {
    u.init(mesh, *x, a, 1);
    DiscreteFunction& uu = dynamic_cast<DiscreteFunction&>(*u.f);
    uu.x = x;
  }

  solved = true;
  end();
}
//-----------------------------------------------------------------------------
void LinearPDE::solve(Function& u0, Function& u1)
{
  // Solve system
  Function u;
  solve(u);

  // Extract sub functions
  u0 = u[0];
  u1 = u[1];
}
//-----------------------------------------------------------------------------
void LinearPDE::solve(Function& u0, Function& u1, Function& u2)
{
  // Solve system
  Function u;
  solve(u);

  // Extract sub functions
  u0 = u[0];
  u1 = u[1];
  u2 = u[2];
}
//-----------------------------------------------------------------------------
